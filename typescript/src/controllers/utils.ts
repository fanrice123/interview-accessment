import { RequestHandler } from 'express';
import { validationResult } from 'express-validator';
import { BAD_REQUEST } from 'http-status-codes';
import Logger from '../config/logger';


/**
 * Generates a middleware to retreive 
 * validation result generated by previous middleware.
 * 
 * @param logger file scoped logger
 */
const validationResultHandler: (logger: Logger) => RequestHandler = (logger) => (async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const statusCode = BAD_REQUEST;
    logger.info(`${statusCode}: ${req.url}`);
    return res.status(statusCode).json({ errors: errors.array() });
  }

  return next();
});


export { validationResultHandler };
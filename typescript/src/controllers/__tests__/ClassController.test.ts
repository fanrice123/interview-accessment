// import { mockRequest, mockResponse } from '../../testUtils/Interceptor';
import axios from 'axios';
import AxiosMockAdapter from 'axios-mock-adapter';
import { Model } from 'sequelize';
import { Class, Subject, Student, Teacher, ClassTeacherStudent, sequelize } from '../../models';
import supertest from 'supertest';
import App from '../../app';
import { BAD_REQUEST, NO_CONTENT, OK } from 'http-status-codes';


const initializeTestDb = async () => {
  const transaction = await sequelize.transaction();
  try {
    const subject = await Subject.create({
      code: 'test-subject-class',
      name: 'test-subject-class'
    }, { transaction });
    const cls = await Class.create({
      code: 'test-code-class',
      name: 'test-class-class',
      subjectId: subject.id
    }, { transaction });
    const teacher = await Teacher.create({
      email: 'teacher.class@email.com',
      name: 'teacher name class'
    }, { transaction });
    const student = await Student.create({
      email: 'student.class@email.com',
      name: 'student name class'
    }, { transaction });
    const clsTeacherStudent = await ClassTeacherStudent.create({
      ClassId: cls.id,
      TeacherId: teacher.id,
      StudentId: student.id,
    }, { transaction });


    await transaction.commit();

    return [ subject, cls, teacher, student, clsTeacherStudent ];
  } catch (err) {
    await transaction.rollback();
    return [];
  }
};

const destroyTestDb = async (models: Model[]) => {
  const transaction = await sequelize.transaction();
  await Promise.all(models.map(async (m) => {
    await m.destroy({ transaction });
  }));
  await transaction.commit();
};


describe('Test Class controller', () => {
  beforeAll(async done => {
    await sequelize.authenticate();
    done();
  });

  afterAll(async done => {
    await sequelize.close();
    done();
  });


  describe('Test class name update', () => {
    let fixtures: Model[];

    beforeEach(async done => {
      fixtures = await initializeTestDb();
      done();
    });

    afterEach(async done => {
      await destroyTestDb(fixtures);
      done();
    });

    test('Modify class name failed due to class not found', async (done) => {
      const client = supertest(App);
      const res = await client.put('/api/class/test-code-not-exist')
        .send({ className: 'test-new-name' });

      // expect(thrownErr.response.status).toEqual(BAD_REQUEST);
      // expect(res.body).toEqual(BAD_REQUEST);
      expect(res.status).toEqual(BAD_REQUEST);
      done();
    });

    test('Modify class name successfully', async (done) => {
      const client = supertest(App);

      const res = await client.put('/api/class/test-code-class')
        .send({ className: 'test-new-name' });

      expect(res.status).toBe(NO_CONTENT);
      done();
    });

  });


  describe('Test get students by class', () => {
    const requestMock = new AxiosMockAdapter(axios, { onNoMatch: 'passthrough' });
    const count = 20;
    const students = [...Array(count).keys()].map((i) => ({
      id: i + 5,
      name: `test name ${i}`,
      email: `test${i}@email.com`
    }));
    const mockRes = {
      count,
      students
      // students: [{ id: 5, name: 'test name', email: 'test@email.com' }]
    };
    let fixtures: Model[] = [];

    beforeEach(async () => {
      fixtures = await initializeTestDb();
    });

    afterEach(async () => {
      await destroyTestDb(fixtures);
    });

    test('Get students without local students', async (done) => {
      // const count = 1;
      requestMock.onGet('http://localhost:5000/students').reply(OK, mockRes);
      const client = supertest(App);
      const res = await client.get('/api/class/test-code-that-dont-exist/students')
        .query({ offset: 0, limit: 100 });

      expect(res.status).toEqual(OK);

      const { body } = res;
      expect(body).toHaveProperty('count', count);
      expect(body).toHaveProperty('students');
      const { students: resStudents } = body;
      expect(resStudents).toBeInstanceOf(Array);
      const expected = [...Array(count).keys()].map((idx) => (
        expect.objectContaining({
          id: idx + 5,
          name: `test name ${idx}`,
          email: `test${idx}@email.com`
        })
      ));
      expect(resStudents).toEqual(
        // expect.arrayContaining(mockRes.students)
        expect.arrayContaining(expected)
      );
      done();
    });

    test('Get students with local students', async (done) => {
      const localStudent = await Student.findOne({ where: { email: 'student.class@email.com' } });
      requestMock.onGet('http://localhost:5000/students').reply(OK, mockRes);
      const client = supertest(App);
      const res = await client.get('/api/class/test-code-class/students')
        .query({ offset: 0, limit: 100 });

      expect(res.status).toEqual(OK);

      const { body } = res;
      expect(body).toHaveProperty('count', count + 1);
      expect(body).toHaveProperty('students');
      const { students: resStudents } = body;
      expect(resStudents).toBeInstanceOf(Array);
      expect(resStudents).toEqual(
        // expect.arrayContaining(mockRes.students)
        expect.arrayContaining([
          expect.objectContaining({
            id: localStudent.id,
            email: localStudent.email,
            name: localStudent.name
          }),
          ...[ ...Array(count).keys() ].map((idx) => (
            expect.objectContaining({
              id: idx + 5,
              name: `test name ${idx}`,
              email: `test${idx}@email.com`
            })
          ))
        ])
      );
      done();
    });
  });

});
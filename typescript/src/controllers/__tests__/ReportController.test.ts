import { Model, Transaction } from 'sequelize';
import { Class, Subject, Teacher, Student, ClassTeacherStudent, sequelize } from '../../models';
import supertest from 'supertest';
import App from '../../app';
import { OK } from 'http-status-codes';


const createStudentAndAddToClass = async (idx: number, teacher: Teacher, cls: Class, transaction: Transaction) => {
  const student = await Student.create({
    email: `student.data.${idx}@email.com`,
    name: `student name data ${idx}`
  }, { transaction });
  const clsTeacherStudent = await ClassTeacherStudent.create({
    ClassId: cls.id,
    TeacherId: teacher.id,
    StudentId: student.id,
  }, { transaction });

  return [ student, clsTeacherStudent ];
};

const createStudentAndAddToClassRange = async (range: number[], teacher: Teacher, cls: Class, transaction: Transaction) => {
  let studentRelatedModels: (Student|ClassTeacherStudent)[] = [];
  const [ from, to ] = range;
  for (let i = from; i != to; ++i) {
    studentRelatedModels = [
      ...studentRelatedModels,
      ...await createStudentAndAddToClass(i, teacher, cls, transaction)
    ];
  }
  return studentRelatedModels;
};

const createTeacher = (idx: number, transaction: Transaction) => (
  Teacher.create({
    email: `teacher.${idx}@email.com`,
    name: `teacher name ${idx}`
  }, { transaction })
);

const creatSubject = (idx: number, transaction: Transaction) => (
  Subject.create({
    code: `test-subject-${idx}`,
    name: `test subject ${idx}`
  }, { transaction })
);

const createClass = (idx: number, subject: Subject, transaction: Transaction) => (
  Class.create({
    code: `test-code-${idx}`,
    name: `test class ${idx}`,
    subjectId: subject.id
  }, { transaction })
);

const initializeTestDb = async () => {
  const transaction = await sequelize.transaction();
  try {
    const subject1 = await creatSubject(1, transaction);
    const subject2 = await creatSubject(2, transaction);
    const subject3 = await creatSubject(3, transaction);
    const subjects = [ subject1, subject2, subject3 ];
    const cls1 = await createClass(1, subject1, transaction);
    const cls2 = await createClass(2, subject1, transaction);
    const cls3 = await createClass(3, subject2, transaction);
    const cls4 = await createClass(4, subject2, transaction);
    const cls5 = await createClass(5, subject2, transaction);
    const cls6 = await createClass(6, subject2, transaction);
    const cls7 = await createClass(7, subject3, transaction);
    const cls8 = await createClass(8, subject2, transaction);
    const classes = [ cls1, cls2, cls3, cls4, cls5, cls6, cls7, cls8 ];
    const teacher1 = await createTeacher(1, transaction);
    const teacher2 = await createTeacher(2, transaction);
    const teacher3 = await createTeacher(3, transaction);
    const teacher4 = await createTeacher(4, transaction);
    const teacher5 = await createTeacher(5, transaction);
    const teacher6 = await createTeacher(6, transaction);
    const teacher7 = await createTeacher(7, transaction);
    const teachers = [ teacher1, teacher2, teacher3, teacher4, teacher5, teacher6, teacher7 ];

    const studentRelatedModels = [
      ...await createStudentAndAddToClassRange([0, 5], teacher1, cls1, transaction),
      ...await createStudentAndAddToClassRange([5, 10], teacher2, cls2, transaction),
      ...await createStudentAndAddToClassRange([10, 15], teacher3, cls2, transaction),
      ...await createStudentAndAddToClassRange([15, 20], teacher4, cls3, transaction),
      ...await createStudentAndAddToClassRange([20, 25], teacher5, cls3, transaction),
      ...await createStudentAndAddToClassRange([25, 30], teacher6, cls4, transaction),
      ...await createStudentAndAddToClassRange([30, 35], teacher6, cls5, transaction),
      ...await createStudentAndAddToClassRange([35, 40], teacher7, cls6, transaction),
      ...await createStudentAndAddToClassRange([40, 45], teacher7, cls7, transaction),
      ...await createStudentAndAddToClassRange([45, 50], teacher7, cls8, transaction),
    ];

    await transaction.commit();

    return [ ...subjects, ...classes, ...teachers, ...studentRelatedModels ];
  } catch (err) {
    await transaction.rollback();
    return [];
  }
};

const destroyTestDb = async (models: Model[]) => {
  const transaction = await sequelize.transaction();
  await Promise.all(models.map(async (m) => {
    await m.destroy({ transaction });
  }));
  await transaction.commit();
};


describe('Test Report Controller', () => {
  beforeAll(async done => {
    await sequelize.authenticate();
    done();
  });

  afterAll(async done => {
    await sequelize.close();
    done();
  });

  describe('Test get workload report', () => {
    let fixtures: Model[];

    beforeEach(async done => {
      fixtures = await initializeTestDb();
      done();
    });

    afterEach(async done => {
      await destroyTestDb(fixtures);
      done();
    });

    test('Get report', async done => {
      const client = supertest(App);
      const res = await client.get('/api/reports/workload').send();

      expect(res.status).toEqual(OK);

      expect(res.body).toEqual(
        // expect.arrayContaining(mockRes.students)
        expect.objectContaining({
          'teacher name 1': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-1',
              subjectName: 'test subject 1',
              numberOfClasses: 1
            }),
          ]),
          'teacher name 2': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-1',
              subjectName: 'test subject 1',
              numberOfClasses: 1
            }),
          ]),
          'teacher name 3': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-1',
              subjectName: 'test subject 1',
              numberOfClasses: 1
            }),
          ]),
          'teacher name 4': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-2',
              subjectName: 'test subject 2',
              numberOfClasses: 1
            }),
          ]),
          'teacher name 5': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-2',
              subjectName: 'test subject 2',
              numberOfClasses: 1
            }),
          ]),
          'teacher name 6': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-2',
              subjectName: 'test subject 2',
              numberOfClasses: 2
            }),
          ]),
          'teacher name 7': expect.arrayContaining([
            expect.objectContaining({
              subjectCode: 'test-subject-2',
              subjectName: 'test subject 2',
              numberOfClasses: 2
            }),
            expect.objectContaining({
              subjectCode: 'test-subject-3',
              subjectName: 'test subject 3',
              numberOfClasses: 1
            }),
          ]),
        })
      );
      done();
    });
  });
});
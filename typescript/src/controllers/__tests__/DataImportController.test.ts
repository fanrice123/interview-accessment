// import { mockRequest, mockResponse } from '../../testUtils/Interceptor';
import { Model, Transaction, Op } from 'sequelize';
import { Class, Subject, Student, Teacher, ClassTeacherStudent, sequelize } from '../../models';
import supertest from 'supertest';
import App from '../../app';
import { NO_CONTENT } from 'http-status-codes';

const createStudentAndAddToClass = async (idx: number, teacher: Teacher, cls: Class, transaction: Transaction) => {
  const student = await Student.create({
    email: `student.data.${idx}@email.com`,
    name: `student name data ${idx}`
  }, { transaction });
  const clsTeacherStudent = await ClassTeacherStudent.create({
    ClassId: cls.id,
    TeacherId: teacher.id,
    StudentId: student.id,
  }, { transaction });

  return [ student, clsTeacherStudent ];
};


const initializeTestDb = async () => {
  const transaction = await sequelize.transaction();
  try {
    const subject = await Subject.create({
      code: 'test-subject-data',
      name: 'test-subject-data'
    }, { transaction });
    const cls = await Class.create({
      code: 'test-code-data',
      name: 'test class data',
      subjectId: subject.id
    }, { transaction });
    const teacher = await Teacher.create({
      email: 'teacher.data@email.com',
      name: 'teacher name data'
    }, { transaction });

    let studentRelatedModels: (Student|ClassTeacherStudent)[] = [];
    for (let i = 0; i != 5; ++i) {
      studentRelatedModels = [
        ...studentRelatedModels,
        ...await createStudentAndAddToClass(i, teacher, cls, transaction)
      ];
    }


    await transaction.commit();

    return [ subject, cls, teacher, ...studentRelatedModels ];
  } catch (err) {
    await transaction.rollback();
    return [];
  }
};

const destroyTestDb = async (models: Model[]) => {
  const transaction = await sequelize.transaction();
  await Promise.all(models.map(async (m) => {
    await m.destroy({ transaction });
  }));
  await transaction.commit();
};


describe('Test Data Import Controller', () => {
  beforeAll(async done => {
    await sequelize.authenticate();
    done();
  });

  afterAll(async done => {
    await sequelize.close();
    done();
  });

  describe('Test upload csv', () => {
    let fixtures: Model[];

    beforeEach(async done => {
      fixtures = await initializeTestDb();
      done();
    });

    afterEach(async done => {
      await destroyTestDb(fixtures);
      await Promise.all([
        Class.destroy({ where: {} }),
        Teacher.destroy({ where: {} }),
        Subject.destroy({ where: {} }),
        Student.destroy({ where: {} }),
        ClassTeacherStudent.destroy({ where: {} }),
      ]);
      done();
    });

    test('Upload new class and teacher', async (done) => {
      const client = supertest(App);
      const res = await client.post('/api/upload').attach('data', 'src/controllers/__tests__/data/data.test.1.csv')

      // expect(thrownErr.response.status).toEqual(BAD_REQUEST);
      // expect(res.status).toEqual(NO_CONTENT);
      expect(res.status).toEqual(NO_CONTENT);

      const cls = await Class.findOne({ where: { code: 'P1-1' }});
      expect(cls).toBeInstanceOf(Class);
      expect(cls.code).toStrictEqual('P1-1');

      const students = await Student.findAll({
        where: {
          email: {
            [Op.in]: ['commonstudent1@gmail.com', 'commonstudent2@gmail.com']
          }
        }
      });

      expect(students).toHaveLength(2);

      const cts = await ClassTeacherStudent.findAll({
        where: {
          StudentId: {
            [Op.in]: students.map(s => s.id)
          }
        }
      });

      expect(cts).toHaveLength(2);

      done();
    });

    test('Upload new class and teacher with student delete', async (done) => {
      const client = supertest(App);
      const res = await client.post('/api/upload').attach('data', 'src/controllers/__tests__/data/data.test.2.csv')

      // expect(thrownErr.response.status).toEqual(BAD_REQUEST);
      // expect(res.status).toEqual(NO_CONTENT);
      expect(res.status).toEqual(NO_CONTENT);

      const cls = await Class.findOne({ where: { code: 'P1-1' } });
      expect(cls).toBeInstanceOf(Class);
      expect(cls.name).toStrictEqual('P1 Integrity');

      const students = await Student.findAll({
        where: {
          email: {
            [Op.in]: ['commonstudent1@gmail.com', 'commonstudent2@gmail.com']
          }
        }
      });

      expect(students).toHaveLength(2);

      const removedStudent = await Student.findOne({ where: { email: 'commonstudent1@gmail.com' }});
      expect(removedStudent.name).toStrictEqual('Common Student 1');

      const teacher = await Teacher.findOne({ where: { email: 'teacher1@gmail.com' }});
      expect(teacher.name).toStrictEqual('Teacher 1');

      const cts = await ClassTeacherStudent.findAll({
        where: {
          StudentId: {
            [Op.in]: students.map(s => s.id)
          }
        }
      });

      expect(cts).toHaveLength(1);

      done();
    });

    test('Upload new class and teacher with different name', async (done) => {
      const client = supertest(App);
      const res = await client.post('/api/upload').attach('data', 'src/controllers/__tests__/data/data.test.3.csv')

      // expect(thrownErr.response.status).toEqual(BAD_REQUEST);
      // expect(res.status).toEqual(NO_CONTENT);
      expect(res.status).toEqual(NO_CONTENT);

      const cls = await Class.findOne({ where: { code: 'P1-1' }});
      expect(cls).toBeInstanceOf(Class);
      expect(cls.code).toStrictEqual('P1-1');
      expect(cls.name).toStrictEqual('P1 Changed');

      const subject = await Subject.findOne({ where: { code: 'MATHS' } });
      expect(subject).toBeInstanceOf(Subject);
      expect(subject.name).toStrictEqual('Math Changed');

      const students = await Student.findAll({
        where: {
          email: {
            [Op.in]: ['commonstudent1@gmail.com', 'commonstudent2@gmail.com']
          }
        }
      });

      expect(students).toHaveLength(2);

      const removedStudent = await Student.findOne({ where: { email: 'commonstudent1@gmail.com' }});
      expect(removedStudent.name).toStrictEqual('Common Student 1 Changed');

      const teacher = await Teacher.findOne({ where: { email: 'teacher1@gmail.com' }});
      expect(teacher.name).toStrictEqual('Teacher 1 Changed');

      const cts = await ClassTeacherStudent.findAll({
        where: {
          StudentId: {
            [Op.in]: students.map(s => s.id)
          }
        }
      });

      expect(cts).toHaveLength(2);
      done();
    });
  });
});
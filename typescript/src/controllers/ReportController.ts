import Express, { RequestHandler } from 'express';
import { OK } from 'http-status-codes';
import { Teacher } from '../models';
import { WorkloadReportUnformattedResult, WorkloadReportResult } from 'WorkLoadReport';
import Logger from '../config/logger';

const ReportController = Express.Router();
const LOG = new Logger('ReportController.js');

/**
 *  Gets workload report of each teachers.
 * 
 * @param req request context
 * @param res response context
 * @param next next middleware
 */
const workloadReportHandler: RequestHandler = async (req, res, next) => {
  const result: WorkloadReportUnformattedResult = {};

  try {
    const teachers = await Teacher.findAll({
      include: [
        {
          association: 'classes',
          include: [{
            association: 'subject'
          }]
        }
      ]
    });

    teachers.forEach(teacher => {
      if (!Object.prototype.hasOwnProperty.call(result, teacher.name)) {
        result[teacher.name] = {};
      }
      const teacherSubjects = result[teacher.name];
      teacher.classes.forEach(cls => {
        const { id: subjectId, code: subjectCode, name: subjectName } = cls.subject;
        if (!Object.prototype.hasOwnProperty.call(teacherSubjects, subjectId)) {
          teacherSubjects[subjectId] = {
            subjectCode,
            subjectName,
            numberOfClasses: 0
          };
        }
        ++teacherSubjects[subjectId].numberOfClasses;
      });
    });

    const formattedResult: WorkloadReportResult = {};
    Object.entries(result).forEach(([teacherName, subjects]) => {
      formattedResult[teacherName] = Object.entries(subjects).map(([, subjectInfo]) => (
        subjectInfo
      ));
    });
    const statusCode = OK;
    LOG.info(`${statusCode}: ${req.url}`);
    return res.status(statusCode).json(formattedResult);

  } catch (err) {
    const error = err as Error;
    LOG.error(`Unexpected error: ${error.message}`);
    return next(err);
  }
}

ReportController.get('/workload', workloadReportHandler);

export default ReportController;

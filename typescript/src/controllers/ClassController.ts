import Express, { RequestHandler } from 'express';
import { body, query, param } from 'express-validator';
import { OK, BAD_REQUEST, NO_CONTENT } from 'http-status-codes';
import { Class, sequelize } from '../models';
import axios, { AxiosResponse } from 'axios';
import Logger from '../config/logger';
import { SerializedStudent, ExternalStudentListingResponse } from 'StudentListing';
import { validationResultHandler } from './utils';

const ClassController = Express.Router();
const LOG = new Logger('StudentListingController.js');


/**
 * Middlewares to validate student listing by class request.
 */
const studentListingValidator = [
  param('classCode').exists({ checkNull: true }),
  query('limit').isNumeric().optional().toInt(),
  query('offset').isNumeric().optional().toInt()
];


/**
 * Middlewares to validate class name update request.
 */
const classNameUpdateValidator = [
  param('classCode').exists({ checkNull: true }),
  body('className').exists().isString()
];


/**
 * Updates class name by class unique code.
 * 
 * @param req request context
 * @param res response context
 * @param next next middleware
 */
const classNameUpdateHandler: RequestHandler = async (req, res, next) => {
  const { params: { classCode: code }, body: { className: name } } = req;

  const transaction = await sequelize.transaction();
  let statusCode = NO_CONTENT;

  try {
    const cls = await Class.findOne({ where: { code }, transaction, lock: transaction.LOCK.UPDATE });

    if (!cls) {
      await transaction.rollback();
      statusCode = BAD_REQUEST;
      LOG.info(`${statusCode}: ${req.url}`);
      return res.sendStatus(statusCode);
    }

    await Class.update({ name }, { where: { id: cls.id }, transaction });

    await transaction.commit();

    LOG.info(`${statusCode}: ${req.url}`);
    return res.sendStatus(statusCode);
  } catch (err) {
    await transaction.rollback();
    const error = err as Error;
    LOG.error(`Unexpected error: ${error.message}`);
    next(err);
  }
};


/**
 * Lists student by class unique code
 *  
 * @param req request context
 * @param res response context
 * @param next next middleware
 */
const studentListingHandler: RequestHandler = async (req, res, next) => {
  const { params: { classCode }, query: { offset: offsetStr, limit: limitStr } } = req;

  try {
    const offset = offsetStr as unknown as number;
    const limit = limitStr as unknown as number;
    let serializedStudents: SerializedStudent[] = [];
    let externalStudentOffset = offset;
    let externalStudentLimit = limit;
    let totalStudentCount = 0;

    const cls = await Class.findOne({ where: { code: classCode } });
    if (cls) {
      const studentCount = await cls.countStudents();
      totalStudentCount = studentCount;
      if (studentCount > offset) {
        const offsetStudentCount = studentCount - offset;

        const students = await cls.getStudents({
          offset,
          limit,
          attributes: [
            'id',
            'email',
            'name'
          ],
        });
        serializedStudents = [...students.map(s => s.toJSON() as SerializedStudent)];
        if (offsetStudentCount < limit) {
          externalStudentLimit = limit - students.length;
        }
      } else {
        externalStudentOffset = offset - studentCount;
      }
    }


    if (externalStudentLimit > 0) {
      const externalStudentsUrl = 'http://localhost:5000/students';
      const params = {
        class: classCode,
        offset: externalStudentOffset,
        limit: externalStudentLimit
      };
      const axiosRes: AxiosResponse<ExternalStudentListingResponse> = await axios.get(externalStudentsUrl, { params });

      const { count: externalCount, students: externalStudents } = axiosRes.data;
      totalStudentCount += externalCount;
      serializedStudents = serializedStudents.concat(externalStudents);
    }

    const statusCode = OK;
    LOG.info(`${statusCode}: ${req.url}`);
    return res.status(statusCode).json({
      count: totalStudentCount,
      students: serializedStudents
    });
  } catch (err) {
    const error = err as Error;
    LOG.error(`Unexpected error: ${error.message}`);
    return next(err);
  }
};

ClassController.put('/:classCode', classNameUpdateValidator, validationResultHandler(LOG), classNameUpdateHandler);
ClassController.get('/:classCode/students', studentListingValidator, validationResultHandler(LOG), studentListingHandler);

export default ClassController;

import Express, { RequestHandler } from 'express';
import { Teacher, Class, Subject, Student, ClassTeacherStudent, sequelize } from '../models';
import { NO_CONTENT } from 'http-status-codes';
import Logger from '../config/logger';
import upload from '../config/multer';
import { convertCsvToJson } from '../utils';
import { body } from 'express-validator';
import { validationResultHandler } from './utils';

const DataImportController = Express.Router();
const LOG = new Logger('DataImportController.js');
 

/**
 * Middlewares to validate data import request.
 */
const dataImportValidator: RequestHandler[] = [
  (req, _res, next) => {
    req.body.data = req.file; // express validator doesn't work with multer middleware, assigns file to body to workaround
    next();
  },
  body('data').exists({ checkNull: true })
]

/**
 * Imports class, teacher, student, subject data.
 * Note: Uploading these data altogether results to data denormalization.
 * There are a few **assumption** I made:
 *  * 1 class can only teach 1 subject
 *  * 1 student in 1 class may have multiple teachers.
 * 
 * @param req request context
 * @param res response context
 * @param next next middleware
 */
const dataImportHandler: RequestHandler = async (req, res, next) => {
  const { file } = req;
  const transaction = await sequelize.transaction();

  try {
    const data = await convertCsvToJson(file.path);


    for (let i = 0; i != data.length; ++i) {
      const {
        classCode,
        classname,
        teacherEmail,
        teacherName,
        studentEmail,
        studentName,
        subjectCode,
        subjectName,
        toDelete
      } = data[i];
      const deleteStudentClass = parseInt(toDelete) === 1;

      const dbLockHandle = { transaction, lock: transaction.LOCK.UPDATE };
      const subjectProp = { code: subjectCode, name: subjectName };
      const subject = await Subject.upsertWithLock(subjectProp, dbLockHandle);

      const classProp = { code: classCode, name: classname, subjectId: subject.id };
      const cls = await Class.upsertWithLock(classProp, dbLockHandle, 'teachers');

      const teacherProp = { email: teacherEmail, name: teacherName };
      const teacher = await Teacher.upsertWithLock(teacherProp, dbLockHandle);

      const studentProp = { email: studentEmail, name: studentName };
      const student = await Student.upsertWithLock(studentProp, dbLockHandle);

      const classTeacherStudent = {
        ClassId: cls.id,
        TeacherId: teacher.id,
        StudentId: student.id
      };

      if (deleteStudentClass) {
        await ClassTeacherStudent.destroy({
          where: classTeacherStudent,
          transaction,
        });
      } else {
        await cls.addTeacherAndStudentWithLock({ teacher, student }, dbLockHandle, true);
      }
    }
    await transaction.commit();
    const statusCode = NO_CONTENT;
    LOG.info(`${statusCode}: ${req.url}`);
    return res.sendStatus(statusCode);
  } catch (err) {
    await transaction.rollback();
    const error = err as Error;
    LOG.error(`Unexpected error: ${error.message}`);
    return next(err);
  }

}

DataImportController.post('/upload', upload.single('data'), dataImportValidator, validationResultHandler(LOG), dataImportHandler);

export default DataImportController;

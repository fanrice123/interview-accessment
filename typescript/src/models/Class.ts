import {
  DataTypes,
  Model,
  Optional,
  Association,
  BelongsToManyGetAssociationsMixin,
  Includeable,
  BelongsToManyCountAssociationsMixin,
} from 'sequelize';
import { BAD_REQUEST } from 'http-status-codes';
import ErrorBase from '../errors/ErrorBase';
import sequelize from '../config/database';
import Teacher from './Teacher';
import Student from './Student';
import ClassTeacherStudent from './ClassTeacherStudent';
import Subject from './Subject';
import { DbLockHandles } from 'Db';


class ClassError extends ErrorBase {
  constructor(message?: string) {
    super(message, BAD_REQUEST, BAD_REQUEST);
    this.name = 'ClassError';
  }
}


interface ClassAttributes {
  id: number;
  code: string;
  name: string;
  subjectId: number;
  teacherCount: number;
}

type ClassCreationAttributes = Optional<ClassAttributes, 'id'|'teacherCount'>;

type ClassUpsertAttributes = ClassCreationAttributes;

type ClassAddTeacherAndStudentAttributes = {
  teacher?: Teacher,
  student?: Student,
  teacherId?: number,
  studentId?: number
};

class Class extends Model<ClassAttributes, ClassCreationAttributes> implements ClassAttributes {
  public id!: number;
  public code!: string;
  public name!: string;
  public subjectId!: number;
  public teacherCount!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public readonly teachers?: Teacher[];
  public readonly students?: Student[];

  public readonly subject!: Subject;

  public getTeachers!: BelongsToManyGetAssociationsMixin<Teacher>;
  public getStudents!: BelongsToManyGetAssociationsMixin<Student>;
  public countStudents!: BelongsToManyCountAssociationsMixin;

  public static associations: {
    teachers: Association<Class, Teacher>;
    students: Association<Class, Student>;
    subject: Association<Class, Subject>;
  };

  public static async upsertWithLock(
    values: ClassUpsertAttributes,
    handle: DbLockHandles = {},
    include?: Includeable | Includeable[]
  ): Promise<Class> {
    const { code } = values;
    const { transaction = null, lock = null } = handle;
    let cls = await this.findOne({ where: { code }, transaction, lock, include });

    if (cls) {
      cls = await cls.update(values, { transaction });
    } else {
      cls = await Class.create(values, { transaction, include });
    }

    return cls;
  }

  /**
   * Assigns teacher and student to this class.
   * 
   * @param values Object containing teacherId/teacher model and studentId/student model
   * @param handle Object containing transaction identifier and lock level identifier
   * @param preload true indicates associations with this action have been eager loaded
   */
  public async addTeacherAndStudentWithLock(values: ClassAddTeacherAndStudentAttributes, handle: DbLockHandles = {}, preload = false): Promise<void> {
    let { teacherId = null, studentId = null } = values;
    const { teacher, student } = values;
    teacherId = teacherId || teacher.id;
    studentId = studentId || student.id;
    const { transaction = null } = handle;
    // eager loading 1:n relations empty result is null instead of empty array
    // bad design
    const teachers = preload ? (this.teachers || []) : await this.getTeachers(handle);

    const existTeacher = teachers.find(t => t.id === teacherId);
    if (!existTeacher) {
      if (teachers.length === 2) { 
        throw new ClassError('Adding new teacher to class which already has 2 teachers');
      }
      this.teacherCount += 1;
    }
    await this.save({ fields: ['name', 'teacherCount'], transaction });
    await ClassTeacherStudent.upsert({
      ClassId: this.id,
      TeacherId: teacherId,
      StudentId: studentId
    }, {
      transaction
    });
  }
}


Class.init({
  id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true, autoIncrement: true, allowNull: false },
  code: { type: DataTypes.STRING, unique: true, allowNull: false },
  name: { type: DataTypes.STRING, allowNull: false },
  subjectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
  teacherCount: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false, defaultValue: 0 }
}, {
  sequelize,
  modelName: 'Class',
  timestamps: true,
  indexes: [
    {
      unique: true,
      fields: ['code']
    }
  ]
});


export { ClassError, ClassAttributes, ClassCreationAttributes, ClassUpsertAttributes };

export default Class;

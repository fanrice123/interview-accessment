import {
  DataTypes,
  Model
} from 'sequelize';
import sequelize from '../config/database';
import Class from './Class';
import Teacher from './Teacher';
import Student from './Student';


interface ClassTeacherStudentAttributes {
  ClassId: number;
  TeacherId: number;
  StudentId: number;
}

type ClassTeacherStudentCreationAttributes = ClassTeacherStudentAttributes;


class ClassTeacherStudent extends Model<ClassTeacherStudentAttributes, ClassTeacherStudentCreationAttributes> implements ClassTeacherStudentAttributes {
  public ClassId!: number;
  public TeacherId!: number;
  public StudentId!: number;

  public readonly createdAt!: Date;
}


ClassTeacherStudent.init({
  ClassId: {
    type: DataTypes.INTEGER.UNSIGNED,
    unique: 'ClassTeacherStudent_Unique',
    primaryKey: true,
    references: { model: Class as typeof Model, key: 'id' }
  },
  TeacherId: {
    type: DataTypes.INTEGER.UNSIGNED,
    unique: 'ClassTeacherStudent_Unique',
    primaryKey: true,
    references: { model: Teacher as typeof Model, key: 'id' }
  },
  StudentId: {
    type: DataTypes.INTEGER.UNSIGNED,
    unique: 'ClassTeacherStudent_Unique',
    primaryKey: true,
    references: { model: Student as typeof Model, key: 'id' }
  },
}, {
  sequelize,
  modelName: 'ClassTeacherStudent',
  tableName: 'ClassTeacherStudent',
  timestamps: true,
  updatedAt: false
});


export { ClassTeacherStudentAttributes, ClassTeacherStudentCreationAttributes };

export default ClassTeacherStudent;
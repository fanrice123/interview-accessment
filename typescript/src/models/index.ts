import Class from './Class';
import Teacher from './Teacher';
import Student from './Student';
import Subject from './Subject';
import ClassTeacherStudent from './ClassTeacherStudent';
import sequelize from '../config/database';
import { Model } from 'sequelize';


Class.belongsToMany(Teacher, { through: { model: ClassTeacherStudent as typeof Model, unique: false }, as: 'teachers' });
Class.belongsToMany(Student, { through: { model: ClassTeacherStudent as typeof Model, unique: false }, as: 'students' });
Teacher.belongsToMany(Class, { through: { model: ClassTeacherStudent as typeof Model, unique: false }, as: 'classes' });
Student.belongsToMany(Class, { through: { model: ClassTeacherStudent as typeof Model, unique: false }, as: 'classes' });
Student.belongsToMany(Teacher, { through: { model: ClassTeacherStudent as typeof Model, unique: false }, as: 'teachers' });

Subject.hasMany(Class, { foreignKey: 'subjectId', sourceKey: 'id', as: 'classes' });
Class.belongsTo(Subject, { foreignKey: 'subjectId', targetKey: 'id', as: 'subject' });


export {
  Class,
  Student,
  Subject,
  Teacher,
  ClassTeacherStudent,
  sequelize,
};

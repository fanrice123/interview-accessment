import {
  Model,
  DataTypes,
  BelongsToManyAddAssociationMixin,
  BelongsToManyGetAssociationsMixin,
  Optional,
  Includeable,
  Association,
} from 'sequelize';
import sequelize from '../config/database';
import Class from './Class';
import Student from './Student';
import { DbLockHandles } from 'Db';


interface TeacherAttributes {
  id: number;
  email: string;
  name: string;
}


type TeacherCreationAttributes = Optional<TeacherAttributes, 'id'>;

type TeacherUpsertAttributes = TeacherCreationAttributes;

class Teacher extends Model<TeacherAttributes, TeacherCreationAttributes> implements TeacherAttributes {
  public id!: number;
  public email!: string;
  public name!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public readonly classes!: Class[];

  public getClasses: BelongsToManyGetAssociationsMixin<Class>;
  public addStudents: BelongsToManyAddAssociationMixin<Student, number>;

  public static associations: {
    classes: Association<Teacher, Class>;
  };

  public static async upsertWithLock(
    values: TeacherUpsertAttributes,
    handle: DbLockHandles = {},
    include?: Includeable | Includeable[]
  ): Promise<Teacher> {
    const { email } = values;
    const { transaction = null, lock = null } = handle;
    let teacher = await this.findOne({ where: { email }, transaction, lock, include });

    if (teacher) {
      const { name } = values;
      teacher = await teacher.update({ name }, { transaction });
    } else {
      teacher = await Teacher.create(values, { transaction, include });
    }

    return teacher;
  }
}


Teacher.init({
  id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true, autoIncrement: true, allowNull: false },
  email: { type: DataTypes.STRING, unique: true, allowNull: false },
  name: { type: DataTypes.STRING, allowNull: false }
}, {
  sequelize,
  modelName: 'Teacher',
  timestamps: true,
  indexes: [
    {
      unique: true,
      fields: ['email']
    }
  ]
});


export { TeacherAttributes, TeacherCreationAttributes, TeacherUpsertAttributes };


export default Teacher;

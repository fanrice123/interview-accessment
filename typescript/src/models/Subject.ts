import {
  DataTypes,
  Model,
  Optional,
  Includeable
} from 'sequelize';
import sequelize from '../config/database';
import Class from './Class';
import { DbLockHandles } from 'Db';

interface SubjectAttributes {
  id: number;
  code: string;
  name: string;
}

type SubjectCreationAttributes = Optional<SubjectAttributes, 'id'>;

type SubjectUpsertAttributes = SubjectCreationAttributes;


class Subject extends Model<SubjectAttributes, SubjectCreationAttributes> implements SubjectAttributes {
  public id!: number;
  public code!: string;
  public name!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public readonly classes!: Class[];

  public static async upsertWithLock(
    values: SubjectUpsertAttributes,
    handle: DbLockHandles = {},
    include?: Includeable | Includeable[]
  ): Promise<Subject> {
    const { code } = values;
    const { transaction = null, lock = null } = handle;
    let subject = await this.findOne({ where: { code }, transaction, lock, include });

    if (subject) {
      subject = await subject.update(values, { transaction });
    } else {
      subject = await Subject.create(values, { transaction, include });
    }

    return subject;
  }
}

Subject.init({
  id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true, autoIncrement: true, allowNull: false },
  code: { type: DataTypes.STRING, unique: true, allowNull: false },
  name: { type: DataTypes.STRING, allowNull: false }
}, {
  sequelize,
  modelName: 'Subject',
  timestamps: true,
  indexes: [
    {
      unique: true,
      fields: ['code']
    }
  ]
});


export default Subject;

import {
  Model,
  DataTypes,
  BelongsToManyAddAssociationMixin,
  BelongsToManyGetAssociationsMixin,
  Optional,
  Includeable,
} from 'sequelize';
import sequelize from '../config/database';
import Class from './Class';
import { DbLockHandles } from 'Db';


interface StudentAttributes {
  id: number;
  email: string;
  name: string;
}


type StudentCreationAttributes = Optional<StudentAttributes, 'id'>;

type StudentUpsertAttributes = StudentCreationAttributes;

class Student extends Model<StudentAttributes, StudentCreationAttributes> implements StudentAttributes {
  public id!: number;
  public email!: string;
  public name!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public getClasses: BelongsToManyGetAssociationsMixin<Class>;
  public addClasses: BelongsToManyAddAssociationMixin<Class, string>;

  public static async upsertWithLock(
    values: StudentUpsertAttributes,
    handle: DbLockHandles = {},
    include?: Includeable | Includeable[]
  ): Promise<Student> {
    const { email } = values;
    const { transaction = null, lock = null } = handle;
    let student = await this.findOne({ where: { email }, transaction, lock, include });

    if (student) {
      student = await student.update(values, { transaction });
    } else {
      student = await Student.create(values, { transaction, include });
    }

    return student;
  }
}


Student.init({
  id: { type: DataTypes.INTEGER.UNSIGNED, primaryKey: true, autoIncrement: true, allowNull: false },
  email: { type: DataTypes.STRING, unique: true, allowNull: false },
  name: { type: DataTypes.STRING, allowNull: false }
}, {
  sequelize,
  modelName: 'Student',
  timestamps: true,
  indexes: [
    {
      unique: true,
      fields: ['email']
    }
  ]
});

export default Student;

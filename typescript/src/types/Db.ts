import { Transaction, LOCK } from 'sequelize';

type DbLockHandles =  { transaction?: Transaction, lock?: LOCK };

export { DbLockHandles };


type WorkloadReportSubjectResult = {
  subjectCode: string,
  subjectName: string,
  numberOfClasses: number
};

type WorkloadReportUnformattedResult = {
  [teacherName: string]: {
    [subjectId: number]: WorkloadReportSubjectResult
  }
};

type WorkloadReportResult = {
  [teacherName: string]: WorkloadReportSubjectResult[],
};

export { WorkloadReportSubjectResult, WorkloadReportResult, WorkloadReportUnformattedResult };
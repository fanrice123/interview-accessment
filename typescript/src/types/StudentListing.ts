
type StudentListingParams = {
  classCode: number,
};

type StudentListingQueries = {
  limit: number,
  offset: number,
};

type SerializedStudent = {
  id: number,
  name: string,
  email: string,
};

type ExternalStudentListingResponse = {
  count: number,
  students: SerializedStudent[]
};

export { StudentListingParams, StudentListingQueries, SerializedStudent, ExternalStudentListingResponse };
import Express from 'express';
import ClassController from './controllers/ClassController';
import DataImportController from './controllers/DataImportController';
import HealthcheckController from './controllers/HealthcheckController';
import ReportController from './controllers/ReportController';

const router = Express.Router();

router.use('/', DataImportController);
router.use('/', HealthcheckController);
router.use('/class', ClassController);
router.use('/reports', ReportController);

export default router;
